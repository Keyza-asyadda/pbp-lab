from django.shortcuts import render, HttpResponseRedirect
from .forms import NoteForm
from lab_2.models import Note

app_url = r'/lab-4'

def index(request):
    return render(request, 'lab4_index.html', {'Notes': Note.objects.all().values(),
    'app_url': app_url})

def add_note(request):
    form = NoteForm(request.POST or None)
    if form.is_valid() and request.method=='POST':
        form.save()
        return HttpResponseRedirect('/lab-4')
    return render(request, 'lab4_form.html', {'form': form.as_ul(), 
    'app_url': app_url})

def note_list(request):
    return render(request, 'lab4_note_list.html', {'Notes': Note.objects.all().values(),
    'app_url': app_url})
