from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    
    class Meta:
        model = Note
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['to'].widget.attrs.update({'id': 'to', 'placeholder': 'Recipient'})
        self.fields['From'].widget.attrs.update({'id': 'from', 'placeholder': 'Your name'})
        self.fields['title'].widget.attrs.update({'id':'title', 'placeholder': 'Title'})
        self.fields['message'].widget.attrs.update({'id':'message', 'placeholder': 'Write your message here...'})