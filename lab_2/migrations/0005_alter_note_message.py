# Generated by Django 3.2.7 on 2021-10-08 15:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0004_rename__from_note_from'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='message',
            field=models.TextField(),
        ),
    ]
