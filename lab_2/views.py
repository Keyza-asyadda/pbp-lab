from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from .models import Note
# Create your views here.


def index(request):
    Notes = Note.objects.all().values()
    response = {'Notes': Notes}
    return render(request, 'index.html', response)

def xml(request):
    Notes = Note.objects.all()
    data = serializers.serialize('xml', Notes)
    return HttpResponse(data, content_type="application/xml")

def json(request):
    Notes = Note.objects.all()
    data = serializers.serialize('json', Notes)
    return HttpResponse(data, content_type='application/json')
