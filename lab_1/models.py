from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here
class Friend(models.Model):
    Name = models.CharField(max_length=30)
    NPM = models.PositiveBigIntegerField()
    Date_of_Birth = models.DateField()
