import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}
void doNothing()
{}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'lab_6',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.grey,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Pag'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String jumlahDitampilkan = '10';
  String urutkanBerdasarkan ='Paling dilihat';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: PopupMenuButton(
					icon: Icon(Icons.menu),
					shape: OutlineInputBorder(
						borderSide: BorderSide(
							color: Colors.grey,
							width: 2,
						)
					),
					itemBuilder: (context) => [
					  PopupMenuItem(
						child: Text("Dashboard"),
					  ),
					  PopupMenuItem(
						child: Text("Rumah Sakit"),
					  ),
					  PopupMenuItem(
						child: Text("Berita"),
					  ),
					  PopupMenuItem(
						child: Text("Forum"),
					  ),
					  PopupMenuItem(
						child: Text("Kuis"),
					  ),
					]
			),
		actions:[
			Row(
			children:[
					OutlineButton(
						onPressed: doNothing,
						child: Text('login'),
					),
				],
			),
		]
	  ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
			Text(
				'Berita Covid-19 Terkini',
				style: const TextStyle(fontSize: 30, color: Colors.grey),
			),
			Card(
				child: Column(
          // color: Colors.black,
          children: <Widget>[
            Row(children: [
              DropdownButton<String>(
                  hint: Text('Urutkan berdasarkan'),
                  elevation: 16,
                  style: const TextStyle(
                    color: Colors.deepPurple
                  ),
                  onChanged: (String? value)
                  {
                    setState(()
                    {
                      urutkanBerdasarkan = value!;
                    });
                  },
                  items: <String>['Paling dilihat', 'Terbaru', 'Paling banyak dikomentari', 'Paling disuka']
                    .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    })
                    .toList(),
                ),
              DropdownButton<String>(
                  hint: Text('Jumlah ditampilkan'),
                  elevation: 16,
                  style: const TextStyle(
                    color: Colors.deepPurple
                  ),
                  onChanged:(String? value)
                  {
                    setState(()
                    {
                      jumlahDitampilkan = value!;
                    });
                  },
                  items: <String>['10', '20', '30']
                    .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    })
                    .toList(),
                ),
            ]),
            createCard('Article', 'Admin'),
            createCard('Article2', 'You'),

          ],
        )
			),
			Card(
        child: Column(children: [
          Text('Article Lainnya'),
          recommendCard(),
          recommendCard(),
        ],)
			),
          ],
        ),
      ),
    );
  }
}
Widget createCard([String? title,String? name, int? viewed, int? liked, int? commented])
{
  viewed ??=0;
  liked ??=0;
  commented ??=0;
  title ??="Your title";
  name ??="Your name";
  DateTime now = DateTime.now();
  return Card(
    child: Column(
      children: <Widget>[
        Text(title),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text("$name, ${now.hour.toString().padLeft(2, "0")}:${now.minute.toString().padLeft(2, "0")} ${now.day.toString().padLeft(2, "0")}/${now.month.toString().padLeft(2,"0")}/${now.year.toString()}"),
            Row(
              children: <Widget>[
                RichText(
                    text: TextSpan(
                      children: [
                        WidgetSpan(
                          child: Icon(Icons.preview, size: 20),
                        ),
                        TextSpan(
                          text: "$viewed",
                        ),
                      ],
                    ),
                  ),
                  RichText(
                    text: TextSpan(
                      children: [
                        WidgetSpan(
                          child: Icon(Icons.favorite, size: 20),
                        ),
                        TextSpan(
                          text: "$liked",
                        ),
                      ],
                    ),
                  ),
                  RichText(
                    text: TextSpan(
                      children: [
                        WidgetSpan(
                          child: Icon(Icons.preview, size: 20),
                        ),
                        TextSpan(
                          text: "$commented",
                        ),
                      ],
                    ),
                  ),
              ],
            ),
          ]
        ),
      ]
    )
  );
}

Widget recommendCard([String? title, String? name])
{
  title ??='This is title';
  name ??='This is author name';
  DateTime now = DateTime.now();
  return Card(
    child: Column(children: [
      Text(title),
      Text("$name, ${now.hour.toString().padLeft(2, "0")}:${now.minute.toString().padLeft(2, "0")} ${now.day.toString().padLeft(2, "0")}/${now.month.toString().padLeft(2,"0")}/${now.year.toString()}"),
      Text(name),
    ],)
  );
}